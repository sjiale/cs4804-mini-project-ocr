import argparse
from tensorflow.keras.models import load_model
from imutils.contours import sort_contours
import numpy as np
import cv2
import imutils

def parse_arguments():
    """
    Construct the argument parser and parse the arguments.
    """
    ap = argparse.ArgumentParser()
    ap.add_argument("-m", "--model", required=True,
                    help="path to English Handwritten Characters dataset")
    ap.add_argument("-i", "--image", required=True,
                    help="path to input text file")
    ap.add_argument("-s", "--size", required=True,
                    help="size of image, 28x28 should input 28")
    args = vars(ap.parse_args())

    return args


def load_and_preprocess_image(image_path):
    image = cv2.imread(image_path)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray, (5, 5), 0)
    edged = cv2.Canny(blurred, 30, 150)
    return image, gray, edged

def find_and_sort_contours(edged):
    cnts = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    return sort_contours(cnts, method="left-to-right")[0]

def prepare_images_for_ocr(contours, gray, image_size):
    chars = []
    for c in contours:
        (x, y, w, h) = cv2.boundingRect(c)
        if (w >= 5 and w <= 150) and (h >= 15 and h <= 120):
            roi = gray[y:y + h, x:x + w]
            thresh = cv2.threshold(roi, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]
            thresh = resize_and_pad(thresh)
            thresh = cv2.resize(thresh, (int(image_size), int(image_size)))
            thresh = thresh.astype("float32") / 255.0
            thresh = np.expand_dims(thresh, axis=-1)
            chars.append((thresh, (x, y, w, h)))
    return chars


def resize_and_pad(image):
    (tH, tW) = image.shape
    if tW > tH:
        image = imutils.resize(image, width=32)
    else:
        image = imutils.resize(image, height=32)
    (tH, tW) = image.shape
    dX = int(max(0, 32 - tW) / 2.0)
    dY = int(max(0, 32 - tH) / 2.0)
    return cv2.copyMakeBorder(image, top=dY, bottom=dY, left=dX, right=dX, borderType=cv2.BORDER_CONSTANT, value=(0, 0, 0))

def predict_characters(chars, model):
    chars = np.array([c[0] for c in chars], dtype="float32")
    preds = model.predict(chars)
    return preds

def get_color(prob):
    """ Return a color based on the probability value. """
    if prob > 0.80:
        return (0, 255, 0)  # Green for high confidence
    elif prob > 0.5:
        return (0, 255, 255)  # Yellow for medium confidence
    else:
        return (0, 0, 255)  # Red for low confidence


def annotate_and_display_image(image, chars, preds):
    boxes = [b[1] for b in chars]
    print(boxes)
    labelNames = "0123456789"
    labelNames += "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    labelNames += "abcdefghijklmnopqrstuvwxyz"
    for (pred, (x, y, w, h)) in zip(preds, boxes):
        i = np.argmax(pred)
        prob = pred[i]
        label = labelNames[i]
        print("[INFO] {} - {:.2f}%".format(label, prob * 100))
        cv2.rectangle(image, (x, y), (x+w, y+h), get_color(prob), 2)
        cv2.putText(image, label, (x - 10, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 1.2, get_color(prob), 2)
    cv2.imshow("Image", image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    
if __name__ == "__main__":
    args = parse_arguments()
    
    # load the model
    print("[INFO] loading handwriting OCR model...")
    model = load_model(args["model"])
    
    image, gray, edged = load_and_preprocess_image(args["image"])
    
    contours = find_and_sort_contours(edged)
    
    chars = prepare_images_for_ocr(contours, gray, args["size"])
    
    preds = predict_characters(chars, model)
    
    annotate_and_display_image(image, chars, preds)
    