# CS4804 Mini-Project OCR

## Code Specifications

1. Language: Python3
2. Install third-party library 
    
    ```pip install -r requirements.txt```

3. Trainning dataset: 
    1. [English Handwritten Characters](https://www.kaggle.com/datasets/dhruvildave/english-handwritten-characters-dataset)   Size: 13.7 MB / 25.2 MB(after unzip)
    2. [Handwritten Digits and English Characters](https://www.kaggle.com/datasets/hrishabhtiwari/handwritten-digits-and-english-characters/) Size: 85 MB / 545.57 MB(after unzip)

    **Please place all unzipped dataset files under *dataset* folder**

4. Usage
    1. Train the model

        ```train-ocr-model.py [-h] -d DATASET [-m MODEL] [-p PLOT] [-s] [-t]```

        Example:

        ```python train-ocr-model.py -d dataset/dataset_mnist.csv -s -m model.keras -p result.png```

    2. Implement OCR by running model

        ```run-ocr-model.py [-h] -m MODEL -i IMAGE -s SIZE```

        Example:

        ```python run-ocr-model.py -m model.keras -i /test_cases/hand_writing/1-image.png -s 28```
