import sys
from PIL import Image
from PIL import UnidentifiedImageError
import pytesseract
import cv2
import numpy as np

def getFilePath() -> str:
    """
    Get the file path from system argument. If there is no argument, use default
    file. 
    """
    # Set default file path
    filepath = 'test_cases/gray_text/1-000000.png'

    # If there is an argument, that may be image path
    if len(sys.argv) >= 2:
        filepath = sys.argv[1]
    
    return filepath


def readImage(filepath: str) -> Image:
    """
    Try to open image in file path. If failed, catch error and exit.
    """
    try:
        image = np.array(Image.open(filepath))
    except FileNotFoundError:
        print("File Not Found Error: the file cannot be found.")
        sys.exit()
    except UnidentifiedImageError:
        print("Unidentified Image Error: image cannot be opened and identified.")
        sys.exit()
    except:
        print("Error: something went wrong.")
        sys.exit()
    
    return image


if __name__ == '__main__':
    filepath = getFilePath()
    image = readImage(filepath)

    # This will print out all support languages.
    # print(pytesseract.get_languages(config=''))

    # Compare set for processing image
    resultBefore = pytesseract.image_to_string(image, lang='eng')
    print("Before process image:")
    print(resultBefore)

    # Processing Image
    image = cv2.fastNlMeansDenoisingColored(image, None, 10, 10, 7, 19)
    image = cv2.medianBlur(image, 3)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # _, image = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

    # Show processed image
    cv2.imshow('Processed Image', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    # Image to String
    resultAfter = pytesseract.image_to_string(image, lang='eng')
    print("After process image:")
    print(resultAfter)

    # If pytesseract get nothing from image after processed, use result 
    # before process image
    if resultAfter == '':
        print("Use result before process image:")
        print(resultBefore)